function myFunction() {
    var x = document.getElementById("myTopnav");
   
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

var slideIndex = [1,1];
var slideId = ["slideshow", "titleslides"]
showSlides(1, 0);
showSlides(1, 1);

function plusSlides(n, no) {
  showSlides(slideIndex[no] += n, no);
}

function currentSlide(n, no) {
  showSlides(slideIndex[no] = n, no);
}

function showSlides(n, no) {
  var i;
  var x = document.getElementsByClassName(slideId[no]);
  var dots = document.getElementsByClassName("dot");
  if (n > x.length) {slideIndex[no] = 1}    
  if (n < 1) {slideIndex[no] = x.length}
  for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
  }
  x[slideIndex[no]-1].style.display = "block";  
  dots[slideIndex[no]-1].className += " active"; 
}

$(function() {
  
    // Vars
    var modBtn  = $('#modBtn'),
        modal   = $('#modal'),
        close   = modal.find('.close'),
        modContent = modal.find('.modal-content');
    
    // open modal when click on open modal button 
    modBtn.on('click', function() {
      modal.css('display', 'block');
      modContent.removeClass('modal-animated-out').addClass('modal-animated-in');
    });
    
    // close modal when click on close button or somewhere out the modal content 
    $(document).on('click', function(e) {
      var target = $(e.target);
      if(target.is(modal) || target.is(close)) {
        modContent.removeClass('modal-animated-in').addClass('modal-animated-out').delay(300).queue(function(next) {
          modal.css('display', 'none');
          next();
        });
      }
    });
    
  });